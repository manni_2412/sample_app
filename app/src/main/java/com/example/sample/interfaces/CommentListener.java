package com.example.sample.interfaces;

import com.example.sample.models.GitHubIssueResponse;

import java.util.ArrayList;

public interface CommentListener {
    void onSuccessIssues(ArrayList<GitHubIssueResponse> responses);

    void onFailureIssues(String message);
}
