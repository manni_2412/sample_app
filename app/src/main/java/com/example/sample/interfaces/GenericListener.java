package com.example.sample.interfaces;

import com.example.sample.models.GitHubIssueResponse;

import java.util.ArrayList;

public interface GenericListener {

    void onSuccessIssues(ArrayList<GitHubIssueResponse> response);

    void onFailureIssues(String message);
}
