package com.example.sample.interfaces;

public interface OnClickIssuesListener {
    void onClickIssue(String issueNumber);
}
