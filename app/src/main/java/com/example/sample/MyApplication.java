package com.example.sample;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.example.sample.network.DataManager;

public class MyApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @SuppressLint("StaticFieldLeak")
    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        mInstance = this;
        DataManager dataManager = DataManager.init(context);
        dataManager.initApiManager();
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
}
