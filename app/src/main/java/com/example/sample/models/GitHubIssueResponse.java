package com.example.sample.models;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class GitHubIssueResponse {
    public String id;

    @SerializedName("number")
    public String issueNumber;

    @SerializedName("title")
    public String issueTitle;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("body")
    public String issueBody;

    @SerializedName("user")
    public User userData;

    @Data
    public static class User {
        @SerializedName("login")
        public String userLogin;

        @SerializedName("id")
        public String userId;

        @SerializedName("node_id")
        public String nodeId;

        @SerializedName("avatar_url")
        public String avatarUrl;
    }
}
