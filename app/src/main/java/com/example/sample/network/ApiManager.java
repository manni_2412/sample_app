package com.example.sample.network;

import com.example.sample.BuildConfig;
import com.example.sample.models.GitHubIssueResponse;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    private static final ApiManager instance = new ApiManager();
    private OkHttpClient.Builder httpClient;
    private ApiInterface authenticatedApiClient;

    private ApiManager() {
        httpClient = getBasicHttpClient();
        authenticatedApiClient = getAuthenticatedRetrofitService();
    }

    public static ApiManager getInstance() {
        return instance;
    }

    private ApiInterface getAuthenticatedRetrofitService() {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BuildConfig.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
        return retrofit.create(ApiInterface.class);
    }

    private static OkHttpClient.Builder getBasicHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(30000, TimeUnit.MILLISECONDS)
                .writeTimeout(20000, TimeUnit.MILLISECONDS);
    }

    public Call<ArrayList<GitHubIssueResponse>> getIssues() {
        return authenticatedApiClient.getGitHubIssues();
    }

    public Call<ArrayList<GitHubIssueResponse>> getComments(String number) {
        return authenticatedApiClient.getComments(number);
    }
}
