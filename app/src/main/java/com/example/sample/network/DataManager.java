package com.example.sample.network;

import android.content.Context;

import com.example.sample.models.GitHubIssueResponse;
import com.example.sample.utils.PreferenceManager;

import java.util.ArrayList;

import retrofit2.Call;

public class DataManager {

    private static DataManager instance;
    private ApiManager apiManager;
    private PreferenceManager mPrefManager;

    private DataManager(Context context) {
        mPrefManager = PreferenceManager.getInstance(context);
    }

    /**
     * Returns the single instance of {@link DataManager} if
     * {@link #init(Context)} is called first
     *
     * @return instance
     */
    public static DataManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Call init() before getInstance()");
        }
        return instance;
    }

    /**
     * Method used to create an instance of {@link DataManager}
     *
     * @param context of the application passed from the {@link}
     * @return instance if it is null
     */
    public synchronized static DataManager init(Context context) {
        if (instance == null) {
            instance = new DataManager(context);
        }
        return instance;
    }

    /**
     * Method to initialize {@link ApiManager} class
     */
    public void initApiManager() {
        apiManager = ApiManager.getInstance();
    }

    public Call<ArrayList<GitHubIssueResponse>> hitGithubIssuesApi() {
        return apiManager.getIssues();
    }

    public Call<ArrayList<GitHubIssueResponse>> hitCommentsApi(String number) {
        return apiManager.getComments(number);
    }

    public void saveIssuesData(ArrayList<GitHubIssueResponse> issues) {
        mPrefManager.setIssuesData(issues);
    }

    //
    public ArrayList<GitHubIssueResponse> getIssuesData() {
        return mPrefManager.getIssuesData();
    }

}
