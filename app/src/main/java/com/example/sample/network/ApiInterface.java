package com.example.sample.network;

import com.example.sample.models.GitHubIssueResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("firebase/firebase-ios-sdk/issues")
    Call<ArrayList<GitHubIssueResponse>> getGitHubIssues();

    @GET("firebase/firebase-ios-sdk/issues/{number}/comments")
    Call<ArrayList<GitHubIssueResponse>> getComments(@Path("number") String number);
}
