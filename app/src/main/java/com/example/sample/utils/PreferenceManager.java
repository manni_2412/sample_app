package com.example.sample.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.sample.models.GitHubIssueResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PreferenceManager {

    public static final String ISSUES_DATA = "issues_data";
    private static PreferenceManager mAppSharedPreferenceInstance;
    private SharedPreferences sharedPref;

    private PreferenceManager(Context context) {
        sharedPref = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static PreferenceManager getInstance(Context context) {
        if (mAppSharedPreferenceInstance == null) {
            synchronized (PreferenceManager.class) {
                if (mAppSharedPreferenceInstance == null)
                    mAppSharedPreferenceInstance = new PreferenceManager(context);
            }
        }
        return mAppSharedPreferenceInstance;
    }


    public int getInt(String key) {
        return sharedPref.getInt(key, 0);
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putLong(key, value);
        editor.putInt(key, value);
        editor.apply();
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);         // Commit the edits!
        editor.apply();
    }

    public String getString(String key) {
        return sharedPref.getString(key, null);
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * Method used to get the user details from the SharedPreference
     *
     * @return object of {@link } which contains all the info about the user
     */

    public boolean getBoolean(String key) {
        return sharedPref.getBoolean(key, false);
    }

    public long getLong(String key) {
        return sharedPref.getLong(key, 0);
    }

    public void clearAllPrefs() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }


    public void setIssuesData(ArrayList<GitHubIssueResponse> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ISSUES_DATA, json);
        editor.apply();
    }

    public ArrayList<GitHubIssueResponse> getIssuesData() {
        Gson gson = new Gson();
        String json = sharedPref.getString(ISSUES_DATA, "");
        Type type = new TypeToken<ArrayList<GitHubIssueResponse>>() {
        }.getType();
        ArrayList<GitHubIssueResponse> data = gson.fromJson(json, type);
        return data;
    }
}
