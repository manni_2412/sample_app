package com.example.sample.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.sample.home.HomeViewModel;

public class DailyAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("DailyAlarmReceiver", "Received");
        HomeViewModel homeViewModel = new HomeViewModel();
        homeViewModel.getIssues();
    }
}
