package com.example.sample.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class StringDateComparator implements Comparator<String> {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    public int compare(String lhs, String rhs) {
        int compareResult = 0;
        try {
            Date arg0Date = dateFormat.parse(lhs);
            Date arg1Date = dateFormat.parse(rhs);
            assert arg0Date != null;
            compareResult = arg0Date.compareTo(arg1Date);
        } catch (ParseException e) {
            e.printStackTrace();
            compareResult = lhs.compareTo(rhs);
        }
        return compareResult;
    }
}
