package com.example.sample.home;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sample.R;
import com.example.sample.adapter.IssuesAdapter;
import com.example.sample.interfaces.GenericListener;
import com.example.sample.interfaces.OnClickIssuesListener;
import com.example.sample.models.GitHubIssueResponse;
import com.example.sample.network.DataManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeActivity extends AppCompatActivity implements GenericListener,
        OnClickIssuesListener {

    private Unbinder unbinder;

    @BindView(R.id.rvIssues)
    RecyclerView rvIssues;

    private HomeViewModel homeViewModel;
    private IssuesAdapter githubIssuesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.setUpGenericListener(this);
        if (DataManager.getInstance().getIssuesData() != null
                && DataManager.getInstance().getIssuesData().size() > 0) {
            setUpIssuesView(DataManager.getInstance().getIssuesData());
        } else {
            homeViewModel.getIssues();
        }
    }

    private void setUpIssuesView(ArrayList<GitHubIssueResponse> list) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvIssues.setLayoutManager(manager);
        githubIssuesAdapter = new IssuesAdapter(this, list, false);
        githubIssuesAdapter.setOnClickIssues(this);
        rvIssues.setAdapter(githubIssuesAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onSuccessIssues(ArrayList<GitHubIssueResponse> response) {
        setUpIssuesView(response);
    }

    @Override
    public void onFailureIssues(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickIssue(String issueNumber) {
        Intent intent = new Intent(this, IssueCommentActivity.class);
        intent.putExtra(IssueCommentActivity.COMMENT_NUMBER, issueNumber);
        startActivity(intent);
    }
}
