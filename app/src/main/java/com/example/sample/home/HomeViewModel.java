package com.example.sample.home;

import androidx.lifecycle.ViewModel;

import com.example.sample.interfaces.CommentListener;
import com.example.sample.interfaces.GenericListener;

public class HomeViewModel extends ViewModel {

    private HomeRepo homeRepo = new HomeRepo();

    public void getIssues() {
        homeRepo.getIssues();
    }

    public void setUpGenericListener(GenericListener listener) {
        homeRepo.setGenericListener(listener);
    }

    public void getComments(String number) {
        homeRepo.getComments(number);
    }

    public void setUpCommentListener(CommentListener commentListener) {
        homeRepo.setCommentListener(commentListener);
    }
}
