package com.example.sample.home;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sample.R;
import com.example.sample.adapter.IssuesAdapter;
import com.example.sample.interfaces.CommentListener;
import com.example.sample.models.GitHubIssueResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class IssueCommentActivity extends AppCompatActivity implements CommentListener {

    public static final String COMMENT_NUMBER = "comment_number";
    @BindView(R.id.rvIssues)
    RecyclerView rvIssues;
    private Unbinder unbinder;
    private HomeViewModel homeViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.setUpCommentListener(this);
        homeViewModel.getComments(getIntent().getStringExtra(COMMENT_NUMBER));
    }

    private void setUpIssuesView(ArrayList<GitHubIssueResponse> list) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvIssues.setLayoutManager(manager);
        IssuesAdapter commentAdapter = new IssuesAdapter(this, list, true);
        rvIssues.setAdapter(commentAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onSuccessIssues(ArrayList<GitHubIssueResponse> responses) {
        setUpIssuesView(responses);
    }

    @Override
    public void onFailureIssues(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
