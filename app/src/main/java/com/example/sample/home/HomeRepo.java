package com.example.sample.home;

import com.example.sample.interfaces.CommentListener;
import com.example.sample.interfaces.GenericListener;
import com.example.sample.models.GitHubIssueResponse;
import com.example.sample.network.DataManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepo {

    private CommentListener commentListener;
    private GenericListener genericListener;

    public void setGenericListener(GenericListener listener) {
        this.genericListener = listener;
    }

    public void setCommentListener(CommentListener listener) {
        this.commentListener = listener;
    }


    public void getIssues() {
        DataManager.getInstance().hitGithubIssuesApi().enqueue(new Callback<ArrayList<GitHubIssueResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GitHubIssueResponse>> call, Response<ArrayList<GitHubIssueResponse>> response) {
//                Collections.sort(response.body()., new StringDateComparator());
                genericListener.onSuccessIssues(response.body());
                DataManager.getInstance().saveIssuesData(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<GitHubIssueResponse>> call, Throwable t) {
                genericListener.onFailureIssues(t.getLocalizedMessage());
            }
        });
    }

    public void getComments(String number) {
        DataManager.getInstance().hitCommentsApi(number).enqueue(new Callback<ArrayList<GitHubIssueResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GitHubIssueResponse>> call, Response<ArrayList<GitHubIssueResponse>> response) {
                commentListener.onSuccessIssues(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<GitHubIssueResponse>> call, Throwable t) {
                commentListener.onFailureIssues(t.getLocalizedMessage());
            }
        });
    }
}
