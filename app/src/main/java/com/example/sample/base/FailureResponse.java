package com.example.sample.base;

public class FailureResponse {

    private int errorCode;
    private CharSequence errorMessage;
    private CharSequence responseType;

    public CharSequence getResponseType() {
        return responseType;
    }

    public void setResponseType(CharSequence responseType) {
        this.responseType = responseType;
    }

    public FailureResponse() {
    }

    public FailureResponse(int errorCode, CharSequence errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public CharSequence getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

