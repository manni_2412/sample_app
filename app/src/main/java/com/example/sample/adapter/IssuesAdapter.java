package com.example.sample.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sample.R;
import com.example.sample.interfaces.OnClickIssuesListener;
import com.example.sample.models.GitHubIssueResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IssuesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final boolean isFromIssues;
    private Context context;
    private ArrayList<GitHubIssueResponse> issueList;
    private OnClickIssuesListener issueListener;

    public IssuesAdapter(Context context, ArrayList<GitHubIssueResponse> issueList,
                         boolean isFromIssues) {
        this.context = context;
        this.issueList = issueList;
        this.isFromIssues = isFromIssues;
    }

    public void setOnClickIssues(OnClickIssuesListener listener) {
        this.issueListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (!isFromIssues) {
            return new GithubIssuesViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.row_issues, parent, false));
        } else {
            return new CommentIssueViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.row_comments, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof GithubIssuesViewHolder) {
            GithubIssuesViewHolder issueViewHolder = (GithubIssuesViewHolder) holder;
            issueViewHolder.tvTitle.setText(issueList.get(position).issueTitle);
            issueViewHolder.tvBody.setText(HtmlCompat.fromHtml(issueList.get(position).issueBody,
                    0));

            issueViewHolder.itemView.setOnClickListener(v ->
                    issueListener.onClickIssue(issueList.get(position).issueNumber));
        } else if (holder instanceof CommentIssueViewHolder) {
            CommentIssueViewHolder commentIssueHolder = (CommentIssueViewHolder) holder;
//            commentIssueHolder.tvUserName.setText(issueList.get(position).userData.userLogin);
            Log.e("anil", "user data = " + issueList.get(position).userData);
            commentIssueHolder.tvBody.setText(HtmlCompat.fromHtml(issueList.get(position).issueBody,
                    0));

        }
    }

    @Override
    public int getItemCount() {
        return issueList.size();
    }

    class GithubIssuesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvBody)
        TextView tvBody;

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        GithubIssuesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class CommentIssueViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvBody)
        TextView tvBody;

        @BindView(R.id.tvUserName)
        TextView tvUserName;

        CommentIssueViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
